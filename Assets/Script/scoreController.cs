﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using System.Linq;
using System.IO;

[System.Serializable]
class HighScore {
	public string name;
	public int highScore;
}

public class scoreController : MonoBehaviour {
	public Text yourScore;
	public Text highScoreText;
	string highScorePath;
	HighScore[] hs = new HighScore[1];
	HighScore[] hsArrays;
	string highScore = "";
	// GameData highScore = new GameData();
	// Use this for initialization
	void Start () {
		yourScore.text = GameControllerPuzzle.getScore().ToString();
		highScorePath = "./highScores.json";
		hs[0] = new HighScore(){
			name = MainMenuData.name,
			highScore = GameControllerPuzzle.getScore(),
		};
		writeFile();
		hsArrays = JsonHelper.FromJson<HighScore>(readFile());
		foreach (HighScore _hs in hsArrays) {
			highScore += _hs.highScore.ToString()+" - "+_hs.name+"\n";
		}
		highScoreText.text = highScore;
	}
	
	string readFile() {
		if (File.Exists(highScorePath)) {
			string highScoreData = File.ReadAllText(highScorePath);
			return highScoreData;
			// highScore = JsonUtility.FromJson<GameData>(highScoreData);
		}
		return null;
	}

	void writeFile() {
		string jsonString = JsonHelper.ToJson(hs);
		if (File.Exists(highScorePath)) {
			hsArrays = JsonHelper.FromJson<HighScore>(readFile());
			if (hsArrays.Length < 5) {
				List<HighScore> hsLists = new List<HighScore>(hsArrays);
				hsLists.Add(hs[0]);
				hsLists = hsLists.OrderByDescending(i=>i.highScore).ToList();
				jsonString = JsonHelper.ToJson(hsLists.ToArray());
				File.WriteAllText(highScorePath, jsonString);		
				return;
			}
			for (int i = 0; i < hsArrays.Length; i++) {
				if (hsArrays[i].highScore < hs[0].highScore) {
					hsArrays[i] = hs[0];
					break;
				}
			}
			jsonString = JsonHelper.ToJson(hsArrays);
			File.WriteAllText(highScorePath, jsonString);	
			return;
		}	
		File.WriteAllText(highScorePath, jsonString);
	}

	// Update is called once per frame
	void Update () {
	
	}
}
